import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class WriteInFile {

    private File createFile(String name){
        File file = new File(name);
        if(file.exists()){
            System.out.println(file.getName() + " already exist, so we rewrite it");
        }else{
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.out.println("File creation error " + e.getMessage());
            }
        }
        return file;
    }

    public void writeIn(List<String> ls, String fileName){
        File fileWithLinks = createFile(fileName);
        try(OutputStream os =  new BufferedOutputStream(
                new FileOutputStream(fileWithLinks.getName())
        )){
            for(String f : ls){
                os.write(f.getBytes(StandardCharsets.UTF_8));
                os.flush();
            }
        }catch (IOException e){
            e.printStackTrace();
        }

    }
}
