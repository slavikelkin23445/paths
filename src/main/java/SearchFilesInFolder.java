import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SearchFilesInFolder {

    private List<String> getFile(File file, List<String> ls){
        for(File f: Objects.requireNonNull(file.listFiles(f -> (!file.getName().equals(".") || !file.getName().equals(".."))))){
            ls.add(f + "\n");
            if(f.isDirectory()) {
                getFile(f,ls);
            }
        }
        return ls;
    }

    public List<String> getAllFiles(String folderPath){
        File folderFile = new File(folderPath);
        return new ArrayList<>(getFile(folderFile, new ArrayList<>()));
    }
}
